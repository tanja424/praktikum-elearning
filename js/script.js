/*
Version of 06.12.15
To see all former changes visit our git:
https://bitbucket.org/tanja424/praktikum-elearning.git
*/




//global variables for user, comments and tags
var savedDataFromLC = {};
var commentCounter = {};
var actualUser = "";

function getCommentsLC(){

    savedDataFromLC = {};
    var commentFromLC = {};
    var tagsFromLC = {};
    commentFromLC.events = [];
    tagsFromLC.events = [];
    commentCounter = {};

    //load all comments from the learning context page
    var loadedData = JSON.parse(lc.get("shared_events", '{}'));

    //sort COMMENT and TAGS from the shared events into the corresponding arrays
    for (var i = 0; i < loadedData.events.length; i++) {
    
        // load comments
        if(loadedData.events[i].category.minor == "COMMENT"){
            commentFromLC.events.push(loadedData.events[i]);
            
        // load tags
        } else if(loadedData.events[i].category.minor == "TAGS") {
            tagsFromLC.events.push(loadedData.events[i]);
        }
    }

    //save the actual logged in user
    actualUser = JSON.parse(lc.get("events", '{"model": "COMPLETE"}')).name;

    //put the commentFromLC into our local savedDataFromLC Format 
    /* {"Some Text 1":[
         {"comment":"too complicated", "rating":2, "source": "www.test.de", "tags": "js, web, javascript", "username": "lcuser"},
         {"comment":"nice paper", "rating":5, "source": "www.test.de", "tags": "js, web, javascript", "username": "lcuser"}
        ], "Some Music 1":[
         {"comment":"very good", "rating":5, "source": "www.test.de", "tags": "js, web, javascript", "username": "lcuser"}
     ]};
    */

    if(commentFromLC.events != undefined){
        //go through all saved comments and get all entities to save them in the global array
        for (var i = 0; i<commentFromLC.events.length; i++) {
            var rating = 0, source="", comment= "", tags = "", username = commentFromLC.events[i].name;

            for (var j = 0; j<commentFromLC.events[i].entities.length; j++) {
                if( commentFromLC.events[i].entities[j].key == "rating"){
                    rating = commentFromLC.events[i].entities[j].value;
                }
                if( commentFromLC.events[i].entities[j].key == "source"){
                    source = commentFromLC.events[i].entities[j].value;
                }
                if( commentFromLC.events[i].entities[j].key == "comment"){
                    comment = commentFromLC.events[i].entities[j].value;
                    //for every new comment update the commentCounter
                    if(commentCounter[source] == undefined){
                        commentCounter[source] = 1;
                    }
                    else{
                        commentCounter[source] = commentCounter[source]+1;
                    }
                }
            }

            //if sourceID is in savedDataFromLC  then add all entities, else create source in savedDataFromLC
            if(savedDataFromLC[source] == undefined){
                savedDataFromLC[source] = [];
            }

            savedDataFromLC[source].push({"comment": comment, "rating": rating, "tags": "", "username":username}); 
        }
    }
    else{
        console.log("ERROR no COMMENT on the Learning Context page");
    }

    if(tagsFromLC.events != undefined) {
        //go through all tags from the global value and save them in the global array
        for(var i=0; i<tagsFromLC.events.length; i++) {
            var source="", tags = "", username = tagsFromLC.events[i].name;

            //go through all entites of the actual tags
            for(var j=0; j<tagsFromLC.events[i].entities.length; j++) {
                if(tagsFromLC.events[i].entities[j].key == "source") {
                    source = tagsFromLC.events[i].entities[j].value;
                }
                if(tagsFromLC.events[i].entities[j].key == "tags") {
                    tags = tagsFromLC.events[i].entities[j].value;
                }
            }

            if(savedDataFromLC[source] == undefined) {
                savedDataFromLC[source] = [];
            }

            var entryFound = false;

            //add tags to the comment from the actual user if there is one, if there is no comment add an empty one
            for (var j = 0; j < savedDataFromLC[source].length; j++) {
                if(savedDataFromLC[source][j].username.indexOf(username) > -1){
                    if(savedDataFromLC[source][j].tags.length == 0){
                        savedDataFromLC[source][j].tags = tags;
                    }
                    else{
                        savedDataFromLC[source][j].tags = savedDataFromLC[source][j].tags + "," + tags;
                    }
                    entryFound = true;
                }
            }

            //create new entry if there is no comment from the actual user
            if(!entryFound){
                savedDataFromLC[source].push({"comment": "", "rating": 0, "tags": tags, "username":username}); 
            }
        }

    } else {
        console.log("ERROR no TAG on the Learning Context page");
    }
}


//toggles the filter options on hovering the searchbar
function toggleSearchbar(){
    if(document.getElementById("searchFilter").style.visibility == "visible"){
        document.getElementById("searchFilter").style.visibility = "hidden";
    }
    else{
        document.getElementById("searchFilter").style.visibility = "visible";
    }
}

//toggles the tag container after clicking the + button
function toggleTagContainer(){
    if(document.getElementById('tagBoxContainer').style.display == "block"){
        document.getElementById('tagBoxContainer').style.display = "none";
    }
    else{
        document.getElementById('tagBoxContainer').style.display = "block";
        document.getElementById("commentBox").style.display="none";
    }
}

//show the search bar on clicking the loupe
function showSearch(){
    document.getElementById("searchIcon").style.visibility = "hidden";
    document.getElementById("searchBar").style.visibility = "visible";
}

//closes the searchbar on scolling the page
$(document).scroll(function() {
    document.getElementById("searchIcon").style.visibility = "visible";
    document.getElementById("searchBar").style.visibility = "hidden";  
    document.getElementById("searchFilter").style.visibility = "hidden";
});


//load star icons based on a numerical rating
function showStars(rating){
    var stars = "";
    for(var j = 0; j<5; j++) {
        var currentStar = "";
        if(rating < j+0.5) {
            currentStar = "star-empty-icon";
        } else if(rating < j+1) {
            currentStar = "star-half-full-icon";
        } else {
            currentStar = "star-full-icon";
        }
        stars = stars + "<img class=\"stars\" src=images/" + currentStar  + ".png>";
    }
    return stars;
}

//save all new tags
function saveTags() {
    var tags = document.getElementById("tagBox").value.replace(/, /g,",").split(",");

    //get the source id from the url
    var id = decodeURI(getUrlVars()["id"]);

    //get the actual time stamp
    var date = new Date().toISOString();
    date = date.substr(0, 10) + " " + date.substr(11, 8);

    //create new event with all needed entities
    var event = new Event("START", date , "WEBBASED", "PRIVATE", "TAGS");
    var entitySource = new Entity("source", id);
    var entityTags = new Entity("tags", tags);
    event.addEntity(entitySource);
    event.addEntity(entityTags);

    //post event to leaning context page
    var json = "[" + event.toJson() + "]";
    var resultTags = lc.post("events", json);

    //hide tagBox again
    document.getElementById("tagBoxContainer").style.display="none";

    //update tags
    document.getElementById("getTags").innerHTML = "";

    //load data from leaning context page to update all tags
    var data = loadJSON();

    //search for the element that we canged right now
    for (var i = 0; i<= data.length - 1; i++) {
        if(data[i].id.indexOf(id) == -1){
            continue;
        }
        else{
            data = data[i];
        }
    }

    //update the tagBox
    for (var i = 0; i < data.tags.length; i++) {
        document.getElementById("getTags").innerHTML += data.tags[i] + ", ";
    }

    //delete the last , in tags list
    document.getElementById("getTags").innerHTML = document.getElementById("getTags").innerHTML.replace(/,\s*$/, "");
}

//save the new comment to the learning context page
function saveComment(){
    var comment = document.getElementById("textBox").value;
    var rating = 0;
    var elements = document.getElementsByClassName("star");

    //look for the first empty star to calculate the actual rating
    for (var i = 0; i < elements.length; i++) {
        if(elements[i].src.indexOf("star-empty-icon.png") > -1){
            rating = i;
            break;
        }
        rating = 5;
    }
    console.log(rating);

    //get the id from the url
    var id = decodeURI(getUrlVars()["id"]);

    //get the actual timestamp
    var date = new Date().toISOString();
    date = date.substr(0, 10) + " " + date.substr(11, 8);

    //create new event with all needed entities
    var event = new Event("START", date , "WEBBASED", "PRIVATE", "COMMENT");
    var entity1 = new Entity("source", id);
    var entity2 = new Entity("rating", rating);
    var entity3 = new Entity("comment", comment);
    
    event.addEntity(entity1);
    event.addEntity(entity2);
    event.addEntity(entity3);

    //post event to learning context page
    var json = "[" + event.toJson() + "]";
    if(rating != 0){
        var result2 = lc.post("events", json);
    }

    //rearrange commentBox to display a "Thank You"
    document.getElementById("commentBox").style.display="none";
    document.getElementById("thankYou").style.display="block";

    //load all data from learning context page to update the comment box 
    var data = loadJSON();

    //look for the element we changed 
    for (var i = 0; i<= data.length - 1; i++) {
        if(data[i].id.indexOf(id) == -1){
            continue;
        }
        else{
            data = data[i];
        }
    }
    
    //display own star rating
    document.getElementById("likert").innerHTML = showStars(rating);

    //update the comment counter
    if(savedDataFromLC[id] != undefined){
        document.getElementsByClassName("commentCounter")[0].innerHTML = document.getElementsByClassName("commentCounter")[0].innerHTML.replace(/\(.*?\)/g,"("+commentCounter[id]+")");
    }
    else{
        //do nothing   
    }    

    //display the new comments
    onloadComments();
}


//on clicking the stars open a textbox to enter the new comment
function clickStars(){

    //if open close the tag container
    document.getElementById("tagBoxContainer").style.display="none";

    var actualComments = savedDataFromLC[getUrlVars()["id"]];
    var allowedToComment = true;
    var numberOfComments = 0;

    if(actualComments != undefined) {
        numberOfComments = actualComments.length;
    }
    
    //check if there is already a rating (because rating is required for a comment)
    for (var i = 0; i < numberOfComments; i++) {
        if((actualComments[i].username == actualUser) && (actualComments[i].rating != 0)){ 
            allowedToComment = false;
        }
    };
    
    // open the comment box and allow user to write a comment
    if(allowedToComment) {

        // find all images inside the hovered div
        var img = $(this).parent().find("img"),
        len = img.length;

        // check if images exist, and color every star that is before the hovered one
        if( len > 0 ){
            for (var i = 0; i < this.id; i++) {
                img[i].className = "star";
                img[i].src = "images/star-full-icon.png";
            };
            for (var i = this.id; i < len; i++) {
                img[i].className = "star";
                img[i].src = "images/star-empty-icon.png";
            };
        } else {
            // no images
        }

        document.getElementById("commentBox").style.display="block";
    }
}

function onload(){

    //if there is a datatype selected search only for this type
    var type = getUrlVars()["datatype"];
    if(type != undefined){     
        //set the selected datatype checkbox as selected again
        type = type.split("+");
        for (var i = 0; i<=type.length - 1; i++) {
            document.getElementById(type[i]).checked = true;
        };
    }
    //else some error no datatype selected
     
    //if there are some keywords display them in the search bar
    if(getUrlVars()["keywords"] != undefined){
        var res = getUrlVars()["keywords"].toString().split("%20"); 
        document.getElementById("keywordInputBox").value = res.toString().replace(/[,+]/g," ");
    }

    //set the refreshtoken to url (by making use of a hidden form input)
    document.getElementById("refreshToken").value = getUrlVars()["rt"];

    //load comments from LC to global variable
    getCommentsLC();
}

function onloadComments(){

    //delete all comments from the comment box to avoid duplicates
    $("#comments").empty();

    //get the id of the source where the comments belong to
    var id = decodeURI(getUrlVars()["id"]);
    var data = loadJSON();

    //get the actual source that is shown right now
    for (var i = 0; i < data.length; i++) {
        if (data[i].id == id){
            data = data[i];
            document.getElementById("pdfTitle").innerHTML = data.title;
        }
    }

    //if there are comments show them
    if(savedDataFromLC[id] != undefined){

        //go through all comments for actual source and display them in the comment box
        for (var i = 0; i< savedDataFromLC[id].length; i++) {
            if (savedDataFromLC[id][i].rating != 0) {
                newListElement =
                             "<div class=\" commentBox \">"
                            +   showStars(savedDataFromLC[id][i].rating)                        
                            +   "<span class=\"commentUsername\">" + savedDataFromLC[id][i].username + " </span><br>"
                            // +   "<div class=\"tagContainer\">"
                            // +       "<span class=\"tagTitle\">Added Tags: </span>"
                            // +       "<span class=\"tags\">"
                            // +           savedDataFromLC[id][i].tags
                            // +       "</span>"
                            // +   "</div>"
                            +   "<div class=commentText>"
                            +       savedDataFromLC[id][i].comment
                            +   "</div>"  
                            + "</div>"

                //insert new element 
                $("#comments").append(newListElement);
            }
        }
    }
}

//on loading the result page
function onloadResults(){
    onload();

    if(getUrlVars()["datatype"] != undefined){
        loadResults(getUrlVars()["datatype"],getUrlVars()["keywords"]);
    }
}

function onloadPdfView(){

    onload();
    onloadComments();

    var data = loadJSON();
    var id = decodeURI(getUrlVars()["id"]);
    var json = "";

    //get the actual source that is shown in the view page
    for (var i = 0; i<= data.length - 1; i++) {
        if(data[i].id.indexOf(id) == -1){
            continue;
        }
        else{
            json = data[i];
        }
    }

    document.getElementById("getTitle").innerHTML = json.title;

    //check if there is data for the actual source that is shown in the view page
    if(savedDataFromLC[id] != undefined){

        //if there is an own rating - find it - show it
        var actualComments = savedDataFromLC[getUrlVars()["id"]];
        for (var i = 0; i < actualComments.length; i++) {
            if((actualComments[i].username == actualUser) && (actualComments[i].rating != 0)){ 
                document.getElementById("likert").innerHTML = showStars(savedDataFromLC[id][i].rating);
                document.getElementById("thankYou").innerHTML = "Already voted.";
                document.getElementById("thankYou").style.display = "block";
            }
        }
    
        //show the actual number of comments
        document.getElementsByClassName("commentCounter")[0].innerHTML = document.getElementsByClassName("commentCounter")[0].innerHTML.replace(/\(.*?\)/g,"("+commentCounter[id]+")");
    }

    //embedd the different sources
    var container = document.getElementById("pdfWrapper");
    
    // load website
    if(json.type == "text/html") {
        var newContent = document.createElement("div");
        newContent.setAttribute("id", "websitePreview");
        newContent.innerHTML =
            "<object type=\"text/html\" data=" + json.url + " style=\"width:100%; height:100%\">"
            +   "<p>backup content</p>"
            +"</object>";
                
    // load pdf
    } else if(json.type == "application/pdf") {
        var newContent = document.createElement("object");
        newContent.data = json.url;
        newContent.type = "application/pdf";
        newContent.width = "100%";
        newContent.height = "100%";
      
        newContent.innerHTML = 
              "<p>"
            +       "You don't have a PDF plugin for this browser."
            +       "However, you can <a href=" + json.url + ">click here to download the PDF file.</a>"
            + "</p>";
            
    // load plain text
    } else if(json.type == "text/plain") {
        var newContent = document.createElement("object");
        newContent.data = json.url;
        newContent.type = "text/plain";
        newContent.width = "100%";
        newContent.height = "100%";
    
    // load image          
    } else if(json.type == "media/image") {
        var newContent = document.createElement("img");   
        newContent.src = json.url; 
        newContent.id = "containerImage"; 
        
    // load youtube video
    } else if(json.type == "media/youtube") {
        var newContent = document.createElement("iframe");
        var linkID = json.url.split("watch?v=")[1];
        newContent.width = "100%";
        newContent.height = "100%";     
        newContent.src = "https://www.youtube.com/embed/" + linkID;
    
    // load video    
    } else if(json.type == "media/video") {
        var newContent = document.createElement("div");
        newContent.innerHTML =
            "<video controls = \"controls\" width = \"100%\" height = \"100%\" src = " + json.url + " name = " + json.title + ">"
            +"</video>";
        
    // load music
    } else if(json.type == "media/audio") {
        var newContent = document.createElement("audio");
        newContent.controls = "controls";
        newContent.src = json.url;
    }
    container.appendChild(newContent);

    //add the tags to the tagBox
    for (var i = 0; i < json.tags.length; i++) {
        document.getElementById("getTags").innerHTML += json.tags[i] + ", ";
    }  

    //delete the last , from the tag box
    document.getElementById("getTags").innerHTML = document.getElementById("getTags").innerHTML.replace(/,\s*$/, "");
}

//function to read out the shared data from the lc webpage
function loadJSON() {   

    var loadedData = JSON.parse(lc.get("shared_events", '{}'));
    var sourcesFromLC = {};
    sourcesFromLC.events = [];

    //filter out all events with type RATEPAGE and put them to the global variable
    for (var i = 0; i < loadedData.events.length; i++) {
        if(loadedData.events[i].category.minor == "RATEPAGE"){
            sourcesFromLC.events.push(loadedData.events[i]);
        }
    }

    //load the comments to add all tags to the sources
    getCommentsLC();

    //if there are events split them into their entities and save them to global variable
    if(sourcesFromLC.events != undefined){
        var data = [];
        for (var i = 0; i<sourcesFromLC.events.length; i++) {           
            var rating = 0, url="", mimetype="", title= "", tags = "", description="";

            for (var j = 0; j<sourcesFromLC.events[i].entities.length; j++) {
                if( sourcesFromLC.events[i].entities[j].key == "rating"){
                    rating = sourcesFromLC.events[i].entities[j].value;
                }
                if( sourcesFromLC.events[i].entities[j].key == "url"){
                    url = sourcesFromLC.events[i].entities[j].value;
                }
                if( sourcesFromLC.events[i].entities[j].key == "mimetype"){
                    mimetype = sourcesFromLC.events[i].entities[j].value;
                }
                if( sourcesFromLC.events[i].entities[j].key == "topic"){
                    title = sourcesFromLC.events[i].entities[j].value;
                }
                if( sourcesFromLC.events[i].entities[j].key == "tags"){
                    tags = sourcesFromLC.events[i].entities[j].value.split(",");;
                }
                if( sourcesFromLC.events[i].entities[j].key == "description"){
                    description = sourcesFromLC.events[i].entities[j].value;
                }
            }
            //get the id and the username of the actual event
            var id = sourcesFromLC.events[i].id;
            var name = sourcesFromLC.events[i].name;

            // add the tags out of the TAGS category to the tags from RATEPAGE
            if(savedDataFromLC[id] != undefined){

                //go through all comments/tags for the actual source
                for (var j = 0; j < savedDataFromLC[id].length; j++) {
                    var savedDataFromLCTags = savedDataFromLC[id][j].tags.split(",");

                    //work case insensitive and check for duplicates - if there is one just dont save it to global variable
                    $.each(savedDataFromLCTags, function( index, value ){
                        if( (tags.indexOf(value.toLowerCase()) == -1) && (value != "")){
                            tags.push(value);
                        }
                    });

                    //calculate the actual rating depending on initial rating (group1) and rating of all comments
                    rating = parseInt(rating);
                    savedDataFromLC[id][j].rating = parseInt(savedDataFromLC[id][j].rating);
                    
                    //check if there is a rating - if yes add it 
                    if(savedDataFromLC[id][j].rating != 0){
                        rating += savedDataFromLC[id][j].rating;
                    }
                }
                rating = rating / (commentCounter[id]+1);
            }
            //push all collected entities to the global variable to work on with it later on
            data.push({"title": title, "type": mimetype, "tags": tags, "url": url, "description": description, "rating": rating, "id": id, "name": name}); 
        }
    }
    else{
        console.log("ERROR no RATEPAGE on the Learning Context page");
    }
    return data;
 }


function loadResults(datatype, keywords) {
    
    //load the JSON from the LC page
    var data = loadJSON();
    var keywordsInTags, keywordsInRatepage = false;

    for (var i = 0; i<= data.length - 1; i++) {

        //if datatype is not requested jump over it
        if(datatype.indexOf(data[i].type) == -1){
            continue;
        }

        //put all tags to lowerCase to ensure some case insensitive search
        for (var j = 0; j < data[i].tags.length; j++) {
            data[i].tags[j] = data[i].tags[j].toLowerCase();
        };

        //split the tag string into an array after each space, comma or +
        var keywordArray = keywords.split(/[ ,+]+/);

        //check keywords from TAGS
        if(savedDataFromLC[data[i].id] != undefined){
            // go through all comment/tag events
            for (var j = 0; j < savedDataFromLC[data[i].id].length; j++) { 
                //go through all keywords
                for (var k = 0; k < keywordArray.length; k++) { 
                    if(savedDataFromLC[data[i].id][j].tags.toString().indexOf(keywordArray[k].toLowerCase()) > -1){
                        keywordsInTags = true;
                    }
                }
            }
        }

        //check keywords from RATEPAGE
        for (var k = 0; k < keywordArray.length; k++) { //go through all keywords
            if (data[i].tags.toString().indexOf(keywordArray[k].toLowerCase()) > -1){
                keywordsInRatepage = true;
            }
        }

        //if not contained in RATEPAGE and TAGS continue with next source
        if(!keywordsInRatepage && !keywordsInTags){
            continue;
        }
        
        //else add the actual source to the result page
        keywordsInTags = false;
        keywordsInRatepage = false;
        json = data[i];
            
        var icon = "";
        var resultTypeClass = "resultBox";
        var stars = "";
        

        //check the type and add the corresponding icon
        if(json.type == "text/html") {
            icon = "<img class=\"resultCategoryIcon\" src=images/icon_globe.png>";
            
        } else if(json.type == "application/pdf") {
            icon = "<img class=\"resultCategoryIcon\" src=images/icon_pdf.png>";
            
        } else if(json.type == "text/plain") {
            icon = "<img class=\"resultCategoryIcon\" src=images/icon_text.png>";   
           
        } else if(json.type == "media/youtube") {
            var previewLink = "http://img.youtube.com/vi/" + json.url.split("watch?v=")[1] + "/1.jpg";
            icon = "<img class=\"previewImage\" src=" + previewLink + ">";
            
        } else if(json.type == "media/video") {
            icon = "<img class=\"resultCategoryIcon\" src=images/icon_video.png>";
           
        } else if(json.type == "media/image") {
            icon = "<img class=\"previewImage\" src=" + json.url + ">";
            
        } else if(json.type == "media/audio") {
            icon = "<img class=\"resultCategoryIcon\" src=images/icon_music.png>";
        }
        
        //show the number of stars according to the numerical rating
        stars = showStars(json.rating);
        
        var resultList = document.getElementById("results");
        var listElements = resultList.getElementsByTagName("li");
        var urlParameters = window.location.href.substring(window.location.href.indexOf('?'));
        var comments = 0;
      
        if (commentCounter[json.id] != undefined){
            comments = commentCounter[json.id];
        }

        json.tags = json.tags.toString()
        json.tags = json.tags.replace(/,/g,', ');

        //crate the new html list element
        newListElement =
            "<li rating=" + json.rating +">"
                + "<div class=\"" + resultTypeClass + "\">"
                    
                    + stars
                    + "<span id=\"" + json.id + "\" class=\"commentCounter\">(" + comments + ")</span>"
                    
                    // Title: create the link to the following site by adding the actual urlParameters to the link 
                    + "<a class=\"resultTitle\" href= \" ./pdfView.html" + urlParameters + "\&id=" + json.id +"\">"  + json.title + "</a>"
                    
                    // Icon
                    + "<div class=\"resultCategory\">"
                    +      icon
                    + "</div>"
        
                    // Content
                    + "<div class=\"resultContent\">"
                    +      "<div class=\"resultSubTitle\">"
                    +           "Tags:"
                    +      "</div>"

                    +      "<div class=\"resultText\">"
                    +           json.tags
                    +      "</div>"
                    + "</div>"
        
                    // Description
                    + "<div class=\"resultDescriptionContainer\">"
                    +      "<div class=\"resultSubTitle\">"
                    +           "Description:"
                    +      "</div>"
                    
                    +      "<div class=\"resultText\">"
                    +           json.description
                    +      "</div>"
                    + "</div>"
                    + "<div class=\"usernameResult\"> "
                    +       "by " + json.name
                    +"</div>"
                + "</div></li>";

        //insert new element by rating
        if($("#results li").length === 0){
            $("#results").append(newListElement);
            continue;
        }

        //sort the list elements according to their star rating
        $("#results li").each(function(){
            var rating = $(this).attr("rating");
            if(json.rating >= rating){
                $(this).before(newListElement);
                return false;
            }
            if($(this).next().length === 0){
                $(this).after(newListElement);
            }
        });
    };   
}

//filters the variables out of the URL and puts them in an object, seperated by a +
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        value = value.replace("%2F", "/");
        value = value.replace("%2C", "");
        if(vars[key] == undefined){
            vars[key] = value;
        }
        else{
            vars[key]= vars[key] + "+" + value;
        }
    });
    return vars;
}
